<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vancouverbolt');

/** MySQL database username */
define('DB_USER', 'bolt');

/** MySQL database password */
define('DB_PASSWORD', 'D8wr7D8q7sKA37mi');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PV8]@hHE4V*U>=HITSArXTW;`6k!>xQVA(D yuNl:V/&x3jV`d8)iffYiVer`:Y^');
define('SECURE_AUTH_KEY',  'A9`7]9sSOZk mU~m`1,R6ev?.lbP!loBqxrP,hSCI)jNRR7|3:d=DG0}{&FP9AQa');
define('LOGGED_IN_KEY',    '7B675Vn8rmG0iJUL0?z/kPc8&7wlB%s@#Y0E}w`+7n5W?@|5;-E:yh=z[<6rA`,E');
define('NONCE_KEY',        'l-rdNON==y)[O??{&@`PXEcG`V Cm(3&z{Dp(zy1@CUB;uqCh*`]In?=bQ;&~6,E');
define('AUTH_SALT',        ' LcI,QKdDxh7`O:n&(GJ8SZ.^AS_usY[QfBEOMG==k|Ue<~aCX]mZ_.$;t|1B7Y~');
define('SECURE_AUTH_SALT', 'Dh0Sk]QMt]>slJe.0egF?YsML2~JQ6?kJv1Mz};kF<z ;_V>?Tnfn!4&Qg&33kW&');
define('LOGGED_IN_SALT',   'b|=SE?qW0i>q}QKHwKq?}GaC<H_*}[VJrCPXh@yaqn/<FXc5UO;kdzS!Q?_z~V/E');
define('NONCE_SALT',       '5gN]e;Wr3z{.kihv5!CaPl<MKR38XV]y$8w:tVH*d#A3d@!n@lteCjG J:z V^3f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
