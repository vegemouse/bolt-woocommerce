<?php
/*
Plugin Name: Custom Hooks
Plugin URI: https://facetwp.com/
Description: A container for custom hooks
Version: 1.0
Author: FacetWP, LLC
*/

function fwp_add_facet_labels() {
?>
<script>
(function($) {
    $(document).on('facetwp-loaded', function() {
        $('.facetwp-facet').each(function() {
            var facet_name = $(this).attr('data-name');
            var facet_label = FWP.settings.labels[facet_name];
            if (
              $('.facet-label[data-for="' + facet_name + '"]').length < 1 &&
              $('.facetwp-facet-' + facet_name).is(':parent')
            ) {
                $(this).before('<h5 class="facet-label" data-for="' + facet_name + '">' + facet_label + '</h5>');
            }
        });
    });
})(jQuery);
</script>
<?php
}
add_action( 'wp_head', 'fwp_add_facet_labels', 100 );


// Find out if the facet-twp with data-value of facet_name has children
// If it does, show all labels with data-for of it
